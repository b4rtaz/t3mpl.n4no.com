![Generate Website Admin Panel](content/image/5841gmvz8i7y8mycchzx.png)

# Don't Write Admin Panel for Small Website, Generate it

Creating admin panel for small website is an expensive thing. Each site has a different structure, and it needs different admin sections. This site has a news section, that doesn't. This site has a landing page with five texts, that site has only one image there. Familiar?

The popular choice to reduce a cost is use Wordpress. But Wordpress is dedicated to blog/news websites. Any deviation from the general purpose of Wordpress means a problem. Of course the market has many plugins and extensions to solve that. Unluckily joining together many elements are difficult and problematic.

So what to do? You may use T3MPL. T3MPL is the generic website editor and the static website generator in one. **When you prepare a template, T3MPL automatically generates editor for your template.**

#### 🎹 Template Contract

Look at **the template contract** bellow. What is it? It is a definition of a data structure in your template. To define that it's needed to create a simple YAML file (template.yaml).

```
dataContract:
  LANDING:
    sections:
      APP:
        properties:
          TITLE:
            type: (text)
            defaultValue: Hello World
          DESCRIPTION:
            type: (markdown)
            defaultFilePath: content/app-description.md
          PREVIEW:
            type: (image)
            defaultFilePath: assets/app-preview.jpg
            width: 1084
            height: 840
...
```

#### 👓 Template

You can use that data directly in a template file. Templates are powered by [handlebars](https://github.com/handlebars-lang/handlebars.js).

```
<div class="landing">
  <h3>{{LANDING.APP.TITLE}}</h3>

  {{{$markdown LANDING.APP.DESCRIPTION}}}

  <img src="{{{$image LANDING.APP.PREVIEW}}}" alt="..." />
</div>
```

#### 💥 Generic Editor

You have everything now. **T3MPL automatically generates an editor for your template.** You may edit your website with live preview now.

<p align="center"><img src="content/image/xjwaxgk21nnqo5ton12x.png" width="270"></p>

#### 🚀 Publishing

When you finish editing, you may want to publish your website. You can do that in two ways.

* **Publish to .zip file** - T3MPL generates your final website to .zip file. You should upload website files to your server. In this case, your server doesn't have special requirements like PHP or Ruby. Generated files should work on the most popular servers like Apache, Nginx and IIS.
* **Use T3MPL Server** - if you want to modify your website in a more professional way, you can use T3MPL Server. It requires Node.js on your server. The server allows you to modify your website directly from the editor. In this case, if you click the "publish" button, all changes will be published immediately.

So you can use T3MPL according to how often you change your site.

If you have a template and a data you may also generate your website by T3MPL Cli.

#### 📣 Summary

T3MPL provides a very simple concept, how to connect the data structure, the template and the editor. With T3MPL you don't care about the admin panel because T3MPL creates the admin panel when you create a website template. This saves time a lot. And money.

At the end, you may also check:

* [Free T3MPL Templates repository](https://github.com/b4rtaz/t3mpl-templates) (many examples)
* [T3MPL Editor repository](https://github.com/b4rtaz/t3mpl-editor)
* [T3MPL Server repository](https://github.com/b4rtaz/t3mpl-server)
* [Online T3MPL Editor](https://t3mpl.n4no.com/)

*— Written by [b4rtaz](https://twitter.com/b4rtaz)*