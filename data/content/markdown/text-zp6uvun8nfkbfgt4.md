### Generic website editor **in browser**

T3MPL is the generic website editor and the static website generator in one. To create a website you need just a browser. Choose a website template and edit it by T3MPL Editor. Export your final website to .zip file and upload it to your server. Also, you may use T3MPL Server and edit your page directly on the server.