#!/bin/bash

npm -v

# repo

git fetch --all
git reset --hard origin/master

# templates

if [ ! -d "public_html/templates" ]
then
	git clone https://github.com/b4rtaz/t3mpl-templates.git ./public_html/templates
fi

cd public_html/templates
git fetch --all
git reset --hard origin/master

cd ../../

# editor

if [ ! -d "editor" ]
then
	git clone https://github.com/b4rtaz/t3mpl-editor.git ./editor
fi

cd editor
git fetch --all
git reset --hard origin/master

npm install
npm run build:prod

cd ../

mv ./editor/dist/t3mpl-editor/* ./public_html/editor/
node ./add_ga_to_editor.js

# build

t3mpl version
t3mpl build --outDir=public_html/ --manifest=public_html/templates/t3mpl-one/template.yaml --data=data/data.json
