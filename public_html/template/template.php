<?php
	$templateName = $_GET['template'];
	$path = '../templates/' . $templateName . '/template.yaml';

	if (!file_exists($path)) {
		header('Location: ../');
		die();
	}

	$templateYaml = file_get_contents($path);
	preg_match('/name: (.*+)/', $templateYaml, $matches);
	$templateTitle = $matches[1];
?>
<!DOCTYPE html>
<html dir="ltr">
<head>
	<meta charset="UTF-8" />
	<title><?php print htmlspecialchars($templateTitle); ?> - T3MPL Editor</title>
	<meta name="description" content="T3MPL is a super simple static website generator. To generate a website you need just a browser (or nodejs)." />
	<meta property="og:title" content="<?php print htmlspecialchars($templateTitle); ?> - Free HTML Template - T3MPL Editor" />
	<meta property="og:description" content="T3MPL is a super simple static website generator. To generate a website you need just a browser (or nodejs)." />
	<meta property="og:image" content="../editor/assets/og-image.png" />

	<link rel="icon" type="image/x-icon" href="../editor/favicon.ico" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<style>
		html, body {margin: 0; padding: 0; overflow: hidden;}
		.root {position: fixed; left: 0; top: 0; display: flex; flex-direction: column; width: 100%; height: 100%; }
		.root .header {padding: 2px 0; background: #DA552F; text-align: center;}
		.root .header img {vertical-align: bottom;}
		.root .header button.close {position: absolute; top: 10px; right: 10px; width: 30px; height: 30px; border-radius: 4px; padding: 0; font-size: 16px; background: #FFF; border: 0; cursor: pointer; text-align: center;}
		.root .header button.close:hover {opacity: 0.8;}
		.root .frame {flex: 1;}
		.root .frame iframe {width: 100%; height: 100%; border: 0;}
	</style>

	<script>
		function removeHeader() {
			var h = document.getElementById('header');
			h.parentElement.removeChild(h);
		}
	</script>
</head>
<body>
	<div class="root">
		<div class="header" id="header">
			<a href="https://www.producthunt.com/posts/mobile-app-website-no-code-builder?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-mobile-app-website-no-code-builder" target="_blank"><img src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=290965&theme=light" alt="Mobile App Website No-Code Builder - Build a website for your mobile app in 60 seconds | Product Hunt" style="width: 213px; height: 46px;" width="213" height="46" /></a>
			<button class="close" onclick="removeHeader();">x</button>
		</div>
		<div class="frame">
			<iframe src="../editor/#manifest=../templates/<?php print htmlspecialchars($templateName); ?>/template.yaml"></iframe>
		</div>
	</div>
</body>
</html>