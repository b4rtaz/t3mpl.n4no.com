const GA_HTML = `<script async src="https://www.googletagmanager.com/gtag/js?id=UA-46308031-8"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-46308031-8');
</script>`;

const fs = require('fs');

const path = './public_html/editor/index.html';
const content = fs.readFileSync(path, {
	encoding: 'utf8'
});

const endIndex = content.indexOf('</body>');

if (endIndex > 0 && content.indexOf(GA_HTML) < 0) {
	let newContent = content.substring(0, endIndex) +
		GA_HTML +
		content.substring(endIndex);

	fs.writeFileSync(path, newContent);
	console.log('GA added.');
} else {
	console.log('GA already installed.');
}
